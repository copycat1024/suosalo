CC = g++
FLAGS = -std=c++17

BIN = suosalo.exe
OBJ = main.o game.o condition.o rule.o suosalo.o

.PHONY: all
all: $(BIN);

%.exe : $(OBJ)
	$(CC) -o $@ $(OBJ) $(FLAGS)

%.o : %.cpp
	$(CC) -c -o $@ $< $(FLAGS)

clean :
	rm $(BIN) $(OBJ)
