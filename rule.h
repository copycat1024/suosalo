#ifndef RULE_H
#define RULE_H

#include <memory>
#include <vector>

#include "condition.h"

namespace game {

class State;

class Action {
   public:
    virtual void act(State&) = 0;
};

class Rule {
   public:
    Rule(ConditionNode&& condition, std::unique_ptr<Action>&& action_if_true,
         std::unique_ptr<Action>&& action_if_false);
    Rule(ConditionNode&& condition, std::unique_ptr<Action>&& action_if_true);
    Rule(std::unique_ptr<Action>&& action_if_true);

    void execute(State& game);

   private:
    ConditionNode _condition;
    std::unique_ptr<Action> _action_t, _action_f;
};

}  // namespace game

#endif