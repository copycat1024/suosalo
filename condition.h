#ifndef COND_H
#define COND_H

#include <memory>

namespace game {

class State;
class CombinedCondition;

class Condition {
   public:
    virtual bool check(const State& game) = 0;
};

class ConditionNode {
   public:
    ConditionNode();
    ConditionNode(std::unique_ptr<Condition>&& condition_ptr);

    bool check(const State& game);

    ConditionNode operator||(ConditionNode&&);

   private:
    std::unique_ptr<Condition> _condition;
};

enum class ConditionOperator { OPS_OR };

class CombinedCondition : public Condition {
   public:
    CombinedCondition(std::unique_ptr<Condition>&& lhs,
                      std::unique_ptr<Condition>&& rhs, ConditionOperator ops);

   private:
    bool check(const State&);

    std::unique_ptr<Condition> _lhs;
    std::unique_ptr<Condition> _rhs;
    ConditionOperator _ops;
};

}  // namespace game

#endif