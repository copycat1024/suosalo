#include "condition.h"

namespace game {

ConditionNode::ConditionNode() : _condition() {}

ConditionNode::ConditionNode(std::unique_ptr<Condition>&& condition_ptr)
    : _condition(std::move(condition_ptr)) {}

bool ConditionNode::check(const State& game) {
    return _condition ? _condition->check(game) : true;
}

ConditionNode ConditionNode::operator||(ConditionNode&& lhs) {
    return ConditionNode(std::make_unique<CombinedCondition>(
        std::move(this->_condition), std::move(lhs._condition),
        ConditionOperator::OPS_OR));
}

CombinedCondition::CombinedCondition(std::unique_ptr<Condition>&& lhs,
                                     std::unique_ptr<Condition>&& rhs,
                                     ConditionOperator ops)
    : _lhs(std::move(lhs)), _rhs(std::move(rhs)), _ops(ops) {}

bool CombinedCondition::check(const State& game) {
    switch (_ops) {
        case ConditionOperator::OPS_OR:
            return _lhs->check(game) || _rhs->check(game);
        default:
            return false;  // TODO: change to throw
    }
}

}  // namespace game