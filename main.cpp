#include <iostream>
#include <memory>

#include "condition.h"
#include "game.h"
#include "rule.h"
#include "suosalo.h"

using namespace game;
using namespace suosalo;

int main() {
    auto game = Game(5, 20);

    game.add_rule(std::make_unique<PrintPlayer>());

    game.add_rule(ConditionNode(std::make_unique<SameDigits>()),
                  std::make_unique<Reverse>());

    game.add_rule(ConditionNode(std::make_unique<DivisibleBy>(7)) ||
                      ConditionNode(std::make_unique<HasDigitTotalEqual>(7)) ||
                      ConditionNode(std::make_unique<ContainDigit>(7)),
                  std::make_unique<ShoutName>("Martti Suosalo"),
                  std::make_unique<ShoutNumber>());

    game.add_rule(std::make_unique<NextTurn>());

    game.run();

    return 0;
}
