#include "suosalo.h"

#include <iomanip>
#include <iostream>

#include "game.h"

namespace suosalo {

void PrintPlayer::act(game::State& game) {
    std::cout << "P:" << std::setw(3) << game.player;
}

void ShoutNumber::act(game::State& game) {
    std::cout << " \"" << game.round << "\"";
}

ShoutName::ShoutName(const std::string& name) : _name(name) {}

void ShoutName::act(game::State& game) { std::cout << " \"" << _name << "\""; }

void Reverse::act(game::State& game) {
    switch (game.direction) {
        case game::Direction::Up:
            game.direction = game::Direction::Down;
            break;

        case game::Direction::Down:
            game.direction = game::Direction::Up;
            break;
    }
}

void NextTurn::act(game::State& game) {
    game.round++;
    switch (game.direction) {
        case game::Direction::Up:
            if (++game.player > game.player_num) {
                game.player = 1;
            }
            break;

        case game::Direction::Down:
            if (--game.player < 1) {
                game.player = game.player_num;
            }
            break;
    }

    std::cout << std::endl;
}

bool SameDigits::check(const game::State& game) {
    int n = game.round;
    int d = n % 10;

    if (n < 10) {
        return false;
    } else {
        while (n > 0) {
            if (n % 10 != d) {
                return false;
            }
            n /= 10;
        }
        return true;
    }
}

DivisibleBy::DivisibleBy(int num) : _num(num) {}

bool DivisibleBy::check(const game::State& game) {
    return game.round % _num == 0;
}

ContainDigit::ContainDigit(int num) : _num(num) {}

bool ContainDigit::check(const game::State& game) {
    int n = game.round;

    while (n > 0) {
        if (n % 10 == _num) {
            return true;
        }
        n /= 10;
    }
    return false;
}

HasDigitTotalEqual::HasDigitTotalEqual(int num) : _num(num) {}

bool HasDigitTotalEqual::check(const game::State& game) {
    int n = game.round;
    int s = 0;

    while (n > 0) {
        s += n % 10;
        if (s > _num) {
            return false;
        }
        n /= 10;
    }
    return s == _num;
}

}  // namespace suosalo
