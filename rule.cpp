#include "rule.h"

namespace game {

Rule::Rule(ConditionNode&& condition, std::unique_ptr<Action>&& action_if_true,
           std::unique_ptr<Action>&& action_if_false)
    : _condition(std::move(condition)),
      _action_t(std::move(action_if_true)),
      _action_f(std::move(action_if_false)) {}

Rule::Rule(ConditionNode&& condition, std::unique_ptr<Action>&& action_if_true)
    : _condition(std::move(condition)),
      _action_t(std::move(action_if_true)),
      _action_f() {}

Rule::Rule(std::unique_ptr<Action>&& action_if_true)
    : _condition(), _action_t(std::move(action_if_true)), _action_f() {}

void Rule::execute(State& game) {
    if (_condition.check(game)) {
        if (_action_t) {
            _action_t->act(game);
        } else {
            // TODO: throw
        }

    } else if (_action_f) {
        _action_f->act(game);
    }
}

}  // namespace game