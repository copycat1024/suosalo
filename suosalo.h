#ifndef SUOSALO_RULE_H
#define SUOSALO_RULE_H

#include "condition.h"
#include "rule.h"

namespace suosalo {

class PrintPlayer : public game::Action {
    void act(game::State&) override;
};

class ShoutNumber : public game::Action {
    void act(game::State&) override;
};

class ShoutName : public game::Action {
   public:
    ShoutName(const std::string& name);

   private:
    void act(game::State&) override;
    std::string _name;
};

class Reverse : public game::Action {
    void act(game::State&) override;
};

class NextTurn : public game::Action {
    void act(game::State&) override;
};

class SameDigits : public game::Condition {
    bool check(const game::State&) override;
};

class DivisibleBy : public game::Condition {
   public:
    DivisibleBy(int num);

   private:
    bool check(const game::State&) override;
    int _num;
};

class ContainDigit : public game::Condition {
   public:
    ContainDigit(int num);

   private:
    bool check(const game::State&) override;
    int _num;
};

class HasDigitTotalEqual : public game::Condition {
   public:
    HasDigitTotalEqual(int num);

   private:
    bool check(const game::State&) override;
    int _num;
};

}  // namespace suosalo

#endif