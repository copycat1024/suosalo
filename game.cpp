#include "game.h"

namespace game {

Game::Game(int player_num, int round_num)
    : _state(player_num, round_num), _rules() {}

void Game::run() {
    while (!this->_state.end()) {
        for (auto& r : _rules) {
            r.execute(this->_state);
        }
    }
}

State::State(int player_num, int round_num)
    : player_num(player_num),
      round_num(round_num),
      round(1),
      player(1),
      direction(Direction::Up) {}

bool State::end() { return round > round_num; }

}  // namespace game
