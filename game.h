#ifndef GAME_H
#define GAME_H

#include <vector>

#include "rule.h"

namespace game {

enum class Direction { Up, Down };

struct State {
    State(int player_num, int round_num);
    bool end();

    const int player_num, round_num;
    int round, player;
    Direction direction;
};

class Game {
   public:
    Game(int player_num, int round_num);
    void run();

    template <class... Args>
    void add_rule(Args&&... args) {
        _rules.emplace_back(std::forward<Args>(args)...);
    }

   private:
    State _state;
    std::vector<Rule> _rules;
};

}  // namespace game

#endif